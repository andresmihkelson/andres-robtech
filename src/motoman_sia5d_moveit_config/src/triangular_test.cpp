#include <moveit/move_group_interface/move_group.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>

#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "move_group_interface_tutorial");
  ros::NodeHandle node_handle;
  ros::AsyncSpinner spinner(1);
  spinner.start();

  bool success;
  moveit::planning_interface::MoveGroup group("sia5");
  geometry_msgs::Pose nextPose;
  moveit::planning_interface::MoveGroup::Plan plan;

  // To visalize plans in Rviz
  ros::Publisher display_publisher = node_handle.advertise<moveit_msgs::DisplayTrajectory>("/move_group/display_planned_path", 1, true);
  moveit_msgs::DisplayTrajectory display_trajectory;

  std::vector<geometry_msgs::Pose> waypoints;

  nextPose.orientation.x = 0.707;
  nextPose.orientation.y = 0;
  nextPose.orientation.z = 0.707;
  nextPose.orientation.w = 0;

  nextPose.position.x = 0.5;
  nextPose.position.y = 0;
  nextPose.position.z = 0.7;
  waypoints.push_back(nextPose);

  nextPose.position.x = 0.5;
  nextPose.position.y = 0;
  nextPose.position.z = 0.5;
  waypoints.push_back(nextPose);


  nextPose.position.x = 0.5;
  nextPose.position.y = 0.3;
  nextPose.position.z = 0.4;
  waypoints.push_back(nextPose);

  nextPose.position.x = 0.5;
  nextPose.position.y = 0;
  nextPose.position.z = 0.7;
  waypoints.push_back(nextPose);

  moveit_msgs::RobotTrajectory trajectory;
  group.computeCartesianPath(waypoints, 0.01, 0.0, trajectory);

  sleep(15);

  ros::shutdown();
  return 0;
}
