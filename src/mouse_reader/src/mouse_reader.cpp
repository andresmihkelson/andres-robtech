#include "mouse_reader/mouse_reader.h"

MouseReader::MouseReader (std::string devPath)
{
  devFileDescriptor = openMouseDevice(devPath.c_str());
}

int MouseReader::openMouseDevice (const char* devPath)
{
  ROS_INFO("Opening device: %s \n", devPath);

  // Open device at devPath for READONLY and get file descriptor
  int fd = open(devPath, O_RDONLY);

  if(fd < 0)
  {
    ROS_ERROR("Failed to open \"%s\"\n", devPath);
    return -1;
  }

  char name[255];
  // Fetch the meaningful (i.e., EVIOCGNAME) name
  if(ioctl(fd, EVIOCGNAME(sizeof(name)), name) < 0)
  {
    ROS_ERROR("\"%s\": EVIOCGNAME failed.", devPath);
    close(fd);
    return -1;
  }

  std::ostringstream sstream;
  sstream << name;
  std::string name_as_string = sstream.str();

  for (int i=0; i < valid_substrings.size(); i++) {
    std::size_t found = name_as_string.find( valid_substrings[i] );
    if (found!=std::string::npos) {
      ROS_INFO("Found '%s'. Starting to read ...\n", name);
      return fd;
    }
  }

  close(fd);
  return -1;
}

bool MouseReader::isReadable()
{
  if (devFileDescriptor < 0) return false;

  return true;
}

void MouseReader::closeMouseDevice ()
{
  ROS_INFO("Closing the mouse device.");
  close(devFileDescriptor);
}

void MouseReader::spin(ros::Publisher &movePublisher,
                  ros::Publisher &clickPublisher,
                  ros::Publisher &scrollPublisher)
{
  struct input_event eventsBuffer[EVENT_BUFFER_SIZE];
  int byteCount, eventCount;
  EventType lastEventType;
  mouse_reader::MouseClickEvent clickMsg;
  mouse_reader::MouseMoveEvent moveMsg;
  mouse_reader::MouseScrollEvent scrollMsg;

  while ( ros::ok() )
  {
    byteCount = read(devFileDescriptor, eventsBuffer, sizeof(struct input_event) * EVENT_BUFFER_SIZE);

    if (byteCount > 0) {
      eventCount = byteCount / sizeof(struct input_event);

      for (int i = 0; i < eventCount; i++) {
        lastEventType = processEvent(&eventsBuffer[i]);

        switch (lastEventType) {
          case clickEvent:
            clickMsg.button = (int32_t) lastButton;
            clickMsg.is_pressed = wasButtonPressed;
            clickPublisher.publish(clickMsg);
          break;
          case moveEvent:
            moveMsg.axis = lastMoveAxis;
            moveMsg.value = lastEventValue;
            moveMsg.integral_x = integrals[0];
            moveMsg.integral_y = integrals[1];
            movePublisher.publish(moveMsg);
          break;
          case scrollEvent:
            scrollMsg.value = lastEventValue;
            scrollMsg.integral_s = integrals[2];
            scrollPublisher.publish(scrollMsg);
          break;
        }
      }

      ros::spinOnce();
    } else {
      ROS_WARN("read() on device file descriptor failed!\n");
      return;
    }
  }
}

MouseReader::EventType MouseReader::processEvent(struct input_event *event) {

  EventType eventType = unknownEvent;

  switch (event->type) {

    // Mouse movement and scroll wheel movement
    case EV_REL:

      // REL_X - movement within X-axis
      if (event->code == 0) {
        lastMoveAxis = 0;
        eventType = moveEvent;
        integrals[0] += event->value;

      // REL_Y - movement within Y-axis
    } else if (event->code == 1) {
        lastMoveAxis = 1;
        eventType = moveEvent;
        integrals[1] += event->value;

      // REL_WHEEL - scroll wheel movement
    } else if (event->code == 8) {
        eventType = scrollEvent;
        integrals[2] += event->value;

      // Unknown code, don't update the value
      } else {
        break;
      }

      lastEventValue = event->value;
    break;

    // Handling clicks
    case EV_KEY:
      lastButton = unknownBtn;

      // Which button?
      switch (event->code) {
        case 272:
          lastButton = leftBtn;
          break;
        case 274:
          lastButton = middleBtn;
          break;
        case 273:
          lastButton = rightBtn;
          break;
      }

      eventType = clickEvent;
      wasButtonPressed = event->value;
    break;
  }

  return eventType;
}


int main(int argc, char* argv[])
{
  ros::init(argc, argv, "mouse_reader");

  // Getting the user-specified path
  ros::NodeHandle nh("~");
  std::string devPath;
  nh.param<std::string>("path", devPath, "");

  // MouseReader object
  MouseReader mouseReader(devPath);

  // Check, if it was properly constructed
  if ( !mouseReader.isReadable() )
  {
    ROS_ERROR("The specified mouse event device is not readable!");
    return -1;
  }

  // Msg publishers
  ros::Publisher pubMouseMoves = nh.advertise<mouse_reader::MouseMoveEvent>("move_events", 100);
  ros::Publisher pubMouseClicks = nh.advertise<mouse_reader::MouseClickEvent>("click_events", 100);
  ros::Publisher pubMouseScrolls = nh.advertise<mouse_reader::MouseScrollEvent>("scroll_events", 100);

  // Event handling loop
  mouseReader.spin(pubMouseMoves, pubMouseClicks, pubMouseScrolls);

  mouseReader.closeMouseDevice();
  return 0;
}
