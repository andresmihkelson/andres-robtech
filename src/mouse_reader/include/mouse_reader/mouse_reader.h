#include <linux/input.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <sstream>
#include <iostream>

#include "ros/ros.h"

#include "mouse_reader/MouseMoveEvent.h"
#include "mouse_reader/MouseClickEvent.h"
#include "mouse_reader/MouseScrollEvent.h"

#ifndef MOUSE_READER_H
#define MOUSE_READER_H

class MouseReader
{
  public:
    enum EventType {clickEvent, moveEvent, scrollEvent, unknownEvent};
    enum ButtonType {leftBtn, middleBtn, rightBtn, unknownBtn};

    MouseReader (std::string devPath);

    int openMouseDevice(const char* devPath);
    bool isReadable();
    void spin(ros::Publisher &movePublisher, ros::Publisher &clickPublisher, ros::Publisher &scrollPublisher);
    void closeMouseDevice();

  protected:
    EventType processEvent(struct input_event *event);

  private:
    const int EVENT_BUFFER_SIZE = 32;
    int devFileDescriptor;
    std::vector<std::string> valid_substrings =
    {
      "mouse",
      "Mouse"
    };

    long long integrals[3] = {0, 0, 0};
    int lastEventValue;
    uint8_t lastMoveAxis;
    ButtonType lastButton;
    bool wasButtonPressed;
};

#endif
